EXAMPLE 4) Hello-world project with huge useless/uselessly parameterised code.

PREPARATION: hello.cpp shall be expanded to hello.i (-E option in g++).
 You may $shrinker hello.cpp, but it will show up the example is already minimal.
Demo instead: $ shrinker hello.i, compare afterwards with shrunken result.

The shrunken result will be in _tmp.txt.cpp.

This demo requires: hello.cpp, hello.i, build.sh.
