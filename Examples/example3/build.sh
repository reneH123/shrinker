#!/bin/sh
g++ -DMY_PRE_PROC1 _tmp.txt.cpp -c > _1.txt 2> _2.txt
if [ $? -ne 0 ]
  then return 1
fi

grep -q Hi _tmp.txt.cpp
if [ $? -ne 0 ]
  then return 1
fi

grep -q "error MYERROR" _tmp.txt.cpp
if [ $? -ne 0 ]
  then return 1
fi

