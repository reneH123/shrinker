﻿Shrinker is a language-independent program line reduction tool. A program with some specified behaviour (aka 'symptom') is attempted to be reduced line-wise or block-wise. Shrinker does not necessarily produce minimal programs. In practice, however, it is often acceptable to obtain programs reduced in that manner. Due to its greedy strategy shrinker may be slower for larger programs.


 REDUCTION
Shrinker will try to remove a line or a block and compare, if the program's symptoms do not change.

 SYMPTOM
A symptom is some tracable behaviour of a considered program, e.g. describable by stdout or stderr. If a specifiable symptom is present then the reduction is proceeded, it is not proceeded otherwise.

 BLOCK
A block denotes a couple of lines in a considered program. If attempted successful, the shorter program wins and further reductions are applied to the reduced program. If, however, a reduction fails that reduction is withdrawn and other reductions are tried. Shrinker applies to incoming programs a greedy binary-block removal strategy with maximal block removal first.


If no symptom is provided, the current working directory is searched for 'symptom.sh' which then will be used as default symptom script. The presence of a symptom and a build script are signaled to the console.


  'build.sh'
The build or Static Analysis process of a given source file/project is MANDATORY and has to be the bash script 'build.sh' returning zero in case of success.


  'symptom.sh'
The description of the symptom is done by the script, and is optional. The check for the presence of the check may be signalled already within 'build.sh'.


EXAMPLE1:

build.sh contains:
    “Build some program/project”
    if [ $? -ne 0 ]
      exit FAILED
    fi
    “Check for SYMPTOM1: program contains no substring 'AAA'”
    “Check for SYMPTOM2: program does not contain 'BBB'”
    “Run built (terminating) program with certain parameters”
    “Check if result dumps to console 'res=115'”

EXAMPLE2:

build.sh contains:
    “Verify heap as specified for incoming program”
    if [ $? -ne 0 ]
      exit FAILED
    fi
    exit 0
