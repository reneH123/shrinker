#include <stdio.h>

int a=6;

int f(){
  return 123;
}

int g(){
  return a;
}

int main(){
  printf("Hello world!\n");
  printf(" f is: %d\n", f());
  printf(" g is: %d\n", g());
  ++a;
  printf(" g is: %d\n", g());
  ++a;
  printf(" g is: %d\n", g());
  return f();
}
