EXAMPLE 2) Still simple hello-world project build with some more constraints.

This is a simple hello-world project that is supposed to build and check for program internal spec and some more text-relevant constraints still without error (not even any reaction). If it builds successfully after shrinking, then the shrunken program is taken, if not shrinking is attempted elsewhere, if any possible.

 In order to run this demo, run: $ shrinker input2.cpp.
The shrunken result (so it is still valid according to the build.sh will be dumped in the same directory in _tmp.txt.cpp, check out the console logs!)

All this demo requires are:

- build.sh
- input2.cpp
