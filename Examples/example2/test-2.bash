#!/bin/bash

SHRINKER=../../Debug/shrinker
INPUT="input2.cpp"


setup()
{
  echo "setup"
  # check there is a shrinker available
  if [ ! -f $SHRINKER ]; then
    echo "ERROR: shrinker expected!"
    exit 1
  fi
  if [ ! -f ./EXPECTED ]; then
    echo "ERROR: EXPECTED missing!"
    exit 2
  fi
  # remove artifacts from the past, if any
  rm -f _BSTDERR.txt _BSTDOUT.txt
  # make a copy of input file
  cp $INPUT _tmp.txt.cpp
}

teardown()
{
  echo "teardown"
  # remove artifacts from the past, if any
  rm -f _BSTDERR.txt _BSTDOUT.txt
  rm -f _tmp.txt.cpp
  rm -f _1.txt _2.txt
  rm -f _tmp.txt.o
}

assertEq()
{
  if [ $1 -ne $2 ]; then
    echo "FAILED: $1 not equals $2!"
    exit 3
  fi
}

main()
{
  echo "main - part"
  $SHRINKER _tmp.txt.cpp
  echo $?
  loc1=$(wc -l _tmp.txt.cpp | awk '{ print $1}')

  # further reduction does not reduce, so it is already a minimal program w.r.t. build.sh
  $SHRINKER _tmp.txt.cpp
  echo $?
  loc2=$(wc -l _tmp.txt.cpp | awk '{ print $1}')

  echo "LOC1:$loc1, LOC2:$loc2"
  assertEq $loc1 $loc2
  assertEq $loc1 7

  # double check with EXPECTED
  diff -q _tmp.txt.cpp EXPECTED
  if [ $? -ne 0 ]; then
    echo "ERROR: reduced program _tmp.txt.cpp does not match EXPECTED ! "
    exit 4
  fi
}

setup
main
teardown

# success
exit 0
