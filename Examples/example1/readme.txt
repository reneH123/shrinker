EXAMPLE 1) Simple hello-world project build.

This is a simple hello-world project that is supposed just to build without error (not even any reaction). If it builds successfully after shrinking, then the shrunken program is taken, if not shrinking is attempted elsewhere, if any possible.

 In order to run this demo, run: $ shrinker input1.cpp.
The shrunken result (so it is still valid according to the build.sh will be dumped in the same directory in _tmp.txt.cpp, check out the console logs!)
