#!/bin/sh
g++ -fpermissive _tmp.txt.cpp -o _hello
if [ $? -ne 0 ]
  then return 1
fi
./_hello > _1.txt 2> _2.txt
grep Hello _1.txt
