/*************************************************
  -- GPL3 License notifier --

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


   René Haberland 
Bugs should be reported to haberland1(at)mail(dot)ru).

 Description : shrinks some language-independent source file
 Date	     : 9th August 2015, Saint Petersburg, Russia
  CALL:  $ shrinker input1.cpp

**************************************************/

#ifndef BRANCH
 #define BRANCH "unknown branch"
#endif
#ifndef COMMIT
 #define COMMIT "unknown commit"
#endif

#define SINGLELINEREMOVAL 1
#define DOUBLELINEREMOVAL 2
#define QUADLINEREMOVAL   4
#define BINARYREMOVAL     8

// BINARYREMOVAL = 2^ 3
#define REMOVAL_MSB       3

#include <stdlib.h>
#include <errno.h>
#include <cstdio>
#include <list>
#include <string.h>

#define CCSCRIPT "build.sh"

#define TMPFILE  "_tmp.txt.cpp"
#define BSTDOUT  "_BSTDOUT.txt"
#define BSTDERR  "_BSTDERR.txt"

std::list<char*> buf_front, buf_back; // doubly-linked list for front and back buffer IO
FILE *f_buildscript; // readonly build script
FILE *f;             // unreserved file handler

char build_cmd[100]; // internal console build command (is dumped to console on startup)
char *f_src_name;    // program flag

unsigned int lines_cnt = 0;
unsigned int range_start, range_width, range_end;

int _finished_field = 0;

int removal_strategy = BINARYREMOVAL;
int attempts_failed = 0;
int attempts_passed = 0;

int init_switches(int, char**);
int init(char **);
void init_buffers();
void cleanup();
void serialize(std::list<char*> buffer, const char *f_name);
void next_range();
void reset_range();
void delete_range();
void update_width();
void restrain_strategy();
void stimulate_strategy();
bool adjust_strategy();


void usage(){
	printf("CALL: shrinker [ SOURCE | -v | -h ]\n\n");
	printf("  René Haberland (haberland1@mail.ru), %s  (%s@%s)", __DATE__, BRANCH,COMMIT);
	printf("\n Shrinker is a language-independent program reduction tool capable of reducing");
	printf(" a given input program line by line with a given symptom if provided.\n\n");

	printf("The build process of a given source file/project is MANDATORY and has to be a bash script returning");
	printf(" zero in case of success and has to be named 'build.sh'\n\n.");
}

int main(int argc, char **argv){
	if (init_switches(argc,argv))
		return 1;
	int exit_code = init(argv);
	if (exit_code)
		return exit_code;
	init_buffers();
	if (lines_cnt >= 2048){
		removal_strategy = SINGLELINEREMOVAL;
	}

	bool _finished = false;  // done with current width
	bool _ready = false;     // only if overall reduction is really done!
	bool _reverted = false;
	int _reductions = 0;
	printf("[%d",(int)buf_back.size()+1);
	fflush(stdout);

	range_start = 0;
	switch (removal_strategy){
		case SINGLELINEREMOVAL: range_width = 1; break;
		case DOUBLELINEREMOVAL: range_width = 2; break;
	    case QUADLINEREMOVAL: range_width = 4; break;
		case BINARYREMOVAL:
			range_width = (buf_back.size() >> 1);
			range_width+= buf_back.size()-(range_width<<1);
			break;
		default: range_width = 1;
	}
	range_end = range_width - 1;

	while(_ready == false){
		// phase 1) brute-force one-line removal attempt
		if (buf_back.empty() == true){
			serialize(buf_front, TMPFILE);
			_ready = true;
			continue;
		}
		// try another range
		if (_reverted == true){
			next_range();
		}
		// reached end
		if (range_start  >= buf_back.size()){
			if (adjust_strategy() == false){
				serialize(buf_front, TMPFILE); // take the last stable version as result
				_ready = true;
				continue;
			}
			reset_range();
		}
		delete_range();
		serialize(buf_back,TMPFILE);

		// phase 2) check if it still builds
		int ret = system(build_cmd);
		exit_code = WEXITSTATUS(ret);
		_reverted = false;
		if (exit_code){
			// restore back buffer and increase line_no
			buf_back.assign(buf_front.begin(), buf_front.end());
			_reverted = true;
			++attempts_failed;
			attempts_passed = 0;
			restrain_strategy();
			serialize(buf_back,TMPFILE); // revert back to last working (only needed for debugging)
			continue;
		}

		attempts_failed = 0; // "success"
		++attempts_passed;
		++_reductions;
		_finished_field = 0;  // clear all last-chance STRATEGIES (when sweep reaches EOF)
		if (range_width >= 5){
			printf(" %d", (int)buf_back.size()+1);
		}else{
			if (_reductions%50 == 0)
				printf("\n %d", (int)buf_back.size()+1);
			else
				switch (removal_strategy){
					case SINGLELINEREMOVAL: printf("."); break;
					case DOUBLELINEREMOVAL: printf(":"); break;
					case QUADLINEREMOVAL: printf("+"); break;
					default: printf("*");
				}
		}
		fflush(stdout);
		stimulate_strategy();
		next_range();

		//  update with a real reduction: TMPFILE <- TMPFILE2
		if (buf_front.size() <= buf_back.size()){
			printf("System ERROR: MISSED REDUCTION STEP, FRONT:%d, BACK:%d", (int)buf_front.size(), (int)buf_back.size());
			fflush(stdout);
		}
		buf_front.assign(buf_back.begin(), buf_back.end());
	} // while

	printf("] Finished shrinking successfully down to %d LOC, result is in '%s'.\n", (int)buf_back.size(), TMPFILE);
	cleanup();

	return EXIT_SUCCESS;
}

int init_switches(int argc, char **argv){
	if ((argc == 2) && (!strcmp(argv[1],"-v") || !strcmp(argv[1],"-h"))){
		usage();
		return 2;
	}
	if (argc == 2 && argv[1][0] != '-'){
		f_src_name = argv[1];
	}
	else{
		usage();
		return 1;
	}
	return 0;
}

/* set up internal commands, switches and file descriptors.  *
 * analyze program arguments.                                */
int init(char **argv){
	printf("- Writing temporarily to reserved file name '%s'\n", TMPFILE);
	sprintf(build_cmd, "sh ./%s > %s 2> %s", CCSCRIPT,BSTDOUT,BSTDERR);
	printf("- Using build commando: '%s'\n", build_cmd);
	f = fopen((const char *)f_src_name, "r");
	if (f == NULL) {
		printf("Error: Could not open source file '%s'!\n", f_src_name);
		return errno;
	}
	fclose(f);

	// keep open the following bash scripts
	f_buildscript = fopen(CCSCRIPT,"r");
	if (f_buildscript == NULL) {
		printf("Error: Could not find script file '%s' !\n", CCSCRIPT);
		return errno;
	} else {
		printf("- Found script file: '%s'.\n", CCSCRIPT);
	}

	return 0;
}

// read TMPFILE and initialize I/O-buffers: buf_front, buf_back
void init_buffers(){
	int read;
	char *_line = NULL;
	size_t _len = 0;

	f = fopen(f_src_name, "r");
	fseek(f, 0L, SEEK_SET);
	while ((read = getline(&_line, &_len, f)) != -1){
		// obtain total text line number first
		++lines_cnt;
	}
	fseek(f, 0L, SEEK_SET);  // init front buffer
	for (unsigned int j=0;j<lines_cnt;j++){
		char *line = NULL;
		getline(&line, &_len, f);
		buf_front.push_back(line);
	}
	fclose(f);

	// initialize back buffer with front buffer content
	buf_back.assign(buf_front.begin(), buf_front.end());
	if (_line)
	   free(_line);
}

void cleanup(){
	if (f_buildscript != NULL)
		fclose(f_buildscript);
	buf_front.clear();
	buf_back.clear();
}

void serialize(std::list<char*> buffer, const char *f_name){
	FILE *f = fopen(f_name, "w");
	for (std::list<char*>::iterator it = buffer.begin(); it != buffer.end(); ++it){
		if (*it!=NULL){
			fprintf(f, "%s", *it);
		}
	}
	fclose(f);
}

void next_range(){
	range_start = range_end + 1;
	range_end = range_start + range_width - 1;
	int buf_len = buf_back.size();
	range_end = (buf_len-1 < range_end)? buf_len-1 : range_end;
}

void update_width(){
	switch (removal_strategy){
		case SINGLELINEREMOVAL: range_width = 1; break;
		case DOUBLELINEREMOVAL: range_width = 2; break;
		case QUADLINEREMOVAL: range_width = 4; break;
		case BINARYREMOVAL: range_width = range_width >> 1; break;
		default: range_width = 1; break;
	}
}

void reset_range(){
	range_start = 0;
	update_width();
	range_end = range_width - 1;
}

void delete_range(){
	std::list<char*>::iterator it = buf_back.begin();
	std::advance(it, range_start);
	int d = range_end - range_start + 1; // this is not range_width near the end!
	d = (d < buf_back.size())? d : buf_back.size();
	for (int i=0;i<d;i++){
		buf_back.erase(it++);
	}
}

void restrain_strategy(){
	switch (removal_strategy){
		case SINGLELINEREMOVAL: break;
		case DOUBLELINEREMOVAL:
			switch (attempts_failed){
				case 0 ... 10: removal_strategy = SINGLELINEREMOVAL; break;
				default: removal_strategy = SINGLELINEREMOVAL;
			}
			break;
		case QUADLINEREMOVAL:
			switch (attempts_failed){
				case 0 ... 10: removal_strategy = SINGLELINEREMOVAL; break;
				default: removal_strategy = DOUBLELINEREMOVAL;
			} break;
		case BINARYREMOVAL:
			switch (attempts_failed){
				case 0 ... 10: removal_strategy = SINGLELINEREMOVAL; break;
				default: removal_strategy = QUADLINEREMOVAL;
			} break;
		default: removal_strategy = SINGLELINEREMOVAL; break;
	}
	update_width();
}

void stimulate_strategy(){
	switch (attempts_passed){
		case 5 ... 10:
			removal_strategy = (removal_strategy >= QUADLINEREMOVAL)? QUADLINEREMOVAL: removal_strategy << 1;
			break;
		default: break; // keep removal_strategy untouched!
	}
	if (attempts_passed > 10 && buf_back.size() < 2048){
		removal_strategy = BINARYREMOVAL;
	}
	update_width();
}

// returns false if there is no removal-width left; true otherwise
bool adjust_strategy(){
	// search for first free width available
	for (int i=0;i<REMOVAL_MSB;i++){
		if (!(_finished_field & (1 << i))){  // if i-th is zero (available), then ..
			_finished_field |= (1 << i);
			removal_strategy = (1 << i);  // .. choose STRATEGY for the next step reduction attempts
			return true;
		}
	}
	return false;
}
