BRANCH:=$(shell git branch | awk '{ print $$2 }')
COMMIT:=$(shell git log -n1 | grep "^commit" | awk '{ print $$2 }' | cut -c1-7)

all:
	mkdir -p Debug
	g++ -g -O0 -o Debug/shrinker src/shrinker.cpp -DBRANCH=\"$(BRANCH)\" -DCOMMIT=\"$(COMMIT)\"

clean:
	rm -rf Debug/shrinker *~
	rm -rf docs/*.aux docs/*.dvi docs/*.log docs/*.pdf

tex:
	cd docs && latex shrinker.tex && pdflatex shrinker.tex && cd ..
