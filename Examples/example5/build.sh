#!/bin/bash
SUCCESS=0
FAILED=2
STDOUT=_1.txt
STDERR=_2.txt

# 1st step) build
g++ -o input5 input5.cpp > $STDOUT 2> $STDERR
if [ $? -ne 0 ]; then
  exit $FAILED
fi

# 2nd step) run  (stdout will be in _1.txt, stderr in _2.txt)
./input5 > $STDOUT 2> $STDERR
if [ $? -ne 123 ]; then
  # assertion: we want that magic number here!
  exit $FAILED
fi

# 3rd step) we only seek the results we are interested in
if [ -s $STDERR ]; then
  # treat all warnings/infos as it was an error
  exit $FAILED
fi

grep -q "g is: 7" $STDOUT
if [ $? -ne 0 ]; then
  # we need this SYMPTOM!
  exit $FAILED
fi

exit $SUCCESS
